package works.lmz.syllabus.http

/**
 * Created by karl on 4/11/15.
 */
class ContentType {

	public final static String TEXT_PLAIN = 'text/plain'
	public final static String TEXT_PLAIN_UTF8 = 'text/plain; charset=UTF-8'

	public final static String APPLICATION_JSON = 'application/json'
	public final static String APPLICATION_JSON_UTF8 = 'application/json; charset=UTF-8'

}
