package works.lmz.syllabus.http

import groovy.transform.CompileStatic
import works.lmz.common.stereotypes.SingletonBean
import works.lmz.syllabus.ResponseCode
import works.lmz.syllabus.SyllabusContext
import works.lmz.syllabus.dispatcher.Dispatcher
import works.lmz.syllabus.errors.TransmissionException
import works.lmz.syllabus.events.EventDispatcher
import org.springframework.web.bind.ServletRequestDataBinder

import javax.inject.Inject

/**
 *
 * @author: Richard Vowles - https://plus.google.com/+RichardVowles
 */
@CompileStatic
@SingletonBean
class GetDispatcher implements Dispatcher {
	@Inject EventDispatcher eventDispatcher

	@Override
	List<String> supports() {
		return ['text/plain']
	}

	protected Object provideObject(SyllabusContext syllabusContext) {
		HttpSyllabusContext context = syllabusContext as HttpSyllabusContext

		// we should now know our object
		if (context.currentHandle.serializeObject == null) {
			return ''
		}

		// Irina pointed this out, too much chance of xss
		Get getMarker = context.currentHandle.instance.getClass().getAnnotation(Get)
		if (allowGet(getMarker, context)) {
			Object obj = context.currentHandle.serializeObject.newInstance()

			ServletRequestDataBinder dataBinder = new ServletRequestDataBinder(obj)
			dataBinder.bind(context.request)

			return obj
		} else {
			throw new TransmissionException('GET not supported for this request', ResponseCode.BAD_REQUEST)
		}
	}

	@Override
	Object dispatch(SyllabusContext syllabusContext) {
		if (syllabusContext instanceof HttpSyllabusContext) {
			HttpSyllabusContext context = syllabusContext as HttpSyllabusContext

			context.requestBody = context.request.queryString

			return eventDispatcher.dispatch(context, this.&provideObject)
		}

		return null
	}

	protected boolean allowGet(Get get, SyllabusContext context) {
		if (!get) return false  // no marker

		Closure getClosure = get.value().newInstance(context.currentHandle.instance, context.currentHandle.instance) as Closure

		return getClosure.call(context)
	}
}
