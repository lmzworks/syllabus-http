package works.lmz.syllabus.http

import groovy.transform.CompileStatic
import works.lmz.syllabus.SyllabusContext
import works.lmz.syllabus.hooks.BeforeEvent
import works.lmz.syllabus.hooks.EventHook
import works.lmz.syllabus.hooks.EventHookException
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Referrer logger event hook implementation logs the X-Angular-Referrer header
 */
@BeforeEvent
@CompileStatic
class ReferrerLoggerEventHook implements EventHook {

	/**
	 * Logger
	 */
	private static final Logger log = LoggerFactory.getLogger(ReferrerLoggerEventHook.class)

	/**
	 * Header to read
	 */
	private static final String ANGULAR_REFERRER_HEADER = 'X-Angular-Referrer'

	/**
	 * Logs the angular referrer header
	 *
	 * @param event is the event handler that is about to be invoked
	 *
	 * @throws EventHookException
	 */
	@Override
	void executeHook(SyllabusContext context) throws EventHookException {
		if (context instanceof HttpSyllabusContext) {
			HttpSyllabusContext hContext = context as HttpSyllabusContext

			String referrer = hContext.request.getHeader(ANGULAR_REFERRER_HEADER)

			if (referrer) {
				log.info("API request started from frontend at $referrer")
			}
		}
	}
}
