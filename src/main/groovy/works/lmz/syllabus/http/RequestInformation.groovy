package works.lmz.syllabus.http

import works.lmz.syllabus.events.Event

import javax.servlet.http.HttpServletRequest

/**
 * Class that is able to extract useful request information from HttpServletRequest.pathInfo
 */
class RequestInformation {

	/**
	 * Default version.
	 */
	public static final String DEFAULT_VERSION = "1"

	/**
	 * Request
	 */
	private String[] uriParts = []

	/**
	 * Initialize data-members
	 *
	 * @param request
	 */
	public RequestInformation( HttpServletRequest request ) {
		String path = request?.pathInfo?.trim()
		if ( !( path == null || path.length() == 0 ) ) {
			// strip first char if it is '/'
			if ( path.charAt( 0 ) == '/' as char) {
				path = path.substring( 1 )
			}
			uriParts = path.split( '/' )
		}
	}

	/**
	 * Get the namespace information, or null when not present
	 */
	protected String getNamespace() {
		return getUrlPartIfPresent( 1, 3 ) ?:
			getUrlPartIfPresent( 0, 2 ) ?:
				Event.DEFAULT_NAMESPACE
	}

	/**
	 * Get action name, or null when not present
	 */
	protected String getAction() {
		return getUrlPartIfPresent( 2, 3 ) ?:
			getUrlPartIfPresent( 1, 2 ) ?:
				getUrlPartIfPresent( 0, 1 )
	}

	/**
	 * Get version information or null when not present
	 */
	protected String getVersion() {
		return getUrlPartIfPresent( 0, 3 ) ?: DEFAULT_VERSION
	}

	/**
	 * Return a certain index if there are minItems or more url parts
	 *
	 * @param pathInfo is the path information
	 * @param index
	 * @param minItems
	 * @return
	 */
	protected String getUrlPartIfPresent( int index, int minItems ) {
		if ( uriParts.length >= minItems ) {
			return uriParts[ index ]
		}
		return null
	}
}
