package works.lmz.syllabus.http

import groovy.transform.CompileStatic
import works.lmz.syllabus.ResponseCode
import works.lmz.syllabus.dispatcher.CentralDispatch
import works.lmz.syllabus.errors.TransmissionException
import works.lmz.syllabus.hooks.EventHookException
import works.lmz.common.jackson.JacksonException
import works.lmz.common.jackson.JacksonHelper
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.web.context.support.SpringBeanAutowiringSupport

import javax.inject.Inject
import javax.servlet.ServletConfig
import javax.servlet.ServletException
import javax.servlet.http.HttpServlet
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * User: marnix
 * Date: 25/03/13
 * Time: 10:59 AM
 *
 * Transmission servlet implementation will dispatch to the correct event handler
 */
@CompileStatic
class SyllabusServlet extends HttpServlet {

	/**
	 * Logger
	 */
	private static final Logger log = LoggerFactory.getLogger(SyllabusServlet.class)

	/**
	 * Event dispatchers
	 */
	@Inject
	private CentralDispatch centralDispatch

	/**
	 * Initialize autowired spring beans
	 */
	@Override
	void init(ServletConfig config) throws ServletException {
		SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, config.servletContext)
	}

	/**
	 * Post handler will interpret payload and dispatch to appropriate object
	 *
	 * @param request is the request object
	 * @param response is the response object
	 */
	@Override
	protected void doPost( HttpServletRequest request, HttpServletResponse response ) {

		// Get the CORS stuff out of the way first!
		addCORSHeaders( request, response )

		RequestInformation rInfo = new RequestInformation( request )

		try {
			// get request
			String contentType = request.getContentType()?.tokenize(';')?.get(0) ?: ContentType.TEXT_PLAIN

			log.debug('Incoming content type {}', contentType)

			HttpSyllabusContext context = new HttpSyllabusContext(request: request, response: response,
			    action: rInfo.action, namespace: rInfo.namespace, version: rInfo.version,
				requestBody: request.reader.text, contentType: contentType)

			// dispatch to appropriate location
			Object responseObject = centralDispatch.dispatch( context )

			if (responseObject == null) {
				writeException(
								response,
								ResponseCode.BAD_REQUEST,
								'{error:\'Bad content type or no response from handler\'}'
				)
				return
			}

			// try to output response - should support JSONP here as well
			try {
				if ( responseObject instanceof String && contentType == ContentType.TEXT_PLAIN ) {
					response.setHeader( 'Content-Type', ContentType.TEXT_PLAIN_UTF8 )
					response.writer.print( responseObject.toString() )
				} else {
					response.setHeader( 'Content-Type', ContentType.APPLICATION_JSON_UTF8 )
					response.writer.print( JacksonHelper.serialize( responseObject ) )
				}

				response.writer.flush()
			} catch (JacksonException jEx) {
				log.error('Unable to serialize json response message', responseObject )
				writeException( response, ResponseCode.BAD_GATEWAY, jEx.message )
			}
		} catch (TransmissionException transEx) {
			// write HTTP error status
			writeException( response, transEx.statusCode, transEx.message )
		} catch ( EventHookException ehEx ) {
			log.info('The execution of this request was stopped because of an exception thrown by a @BeforeEvent event hook', ehEx)
			if ( ehEx.statusCode > 0 ) {
				writeException( response, ehEx.statusCode, ehEx.message )
			}
		}
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response)
	}

	@Override
	protected void doOptions( HttpServletRequest request, HttpServletResponse response ) {
		addCORSHeaders( request, response )
		response.setHeader( 'Access-Control-Allow-Methods', 'POST, GET, OPTIONS' )
		response.setHeader( 'Access-Control-Allow-Headers', 'X-Request-With, accept, authorization, content-type, origin' )
		response.setHeader( 'Access-Control-Max-Age', '1728000' )
	}

	protected void writeException(HttpServletResponse response, int status, String message) {
		response.setStatus(status)
		response.writer.write(message)
		response.writer.flush()
	}

	protected void addCORSHeaders(HttpServletRequest request, HttpServletResponse response ) {
		response.setHeader( 'Access-Control-Allow-Origin', request.getHeader( 'Cookie' ) ?: '*' )
		response.setHeader( 'Access-Control-Allow-Credentials', 'true' )
//		response.setHeader( 'Access-Control-Allow-Headers', request.getHeader( 'Access-Control-Request-Headers' ) ?: 'Authorization' )
//		response.setHeader( 'Access-Control-Allow-Methods', 'POST, GET, OPTIONS' )
	}


}
