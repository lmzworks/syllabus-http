package works.lmz.syllabus.http

import org.junit.Test
import works.lmz.syllabus.events.Event

import javax.servlet.http.HttpServletRequest

/**
 * User: marnix
 * Date: 26/03/13
 * Time: 10:17 AM
 *
 * Test Syllabus servlet functionality
 */
class RequestInformationTest {

	@Test
	public void testGenericMethod() {

		RequestInformation ri = new RequestInformation( fakeRequest( '1/2/3' ) )

		assert ri.getUrlPartIfPresent( 0, 0 ) == '1'
		assert ri.getUrlPartIfPresent( 0, 1 ) == '1'
		assert ri.getUrlPartIfPresent( 0, 2 ) == '1'
		assert ri.getUrlPartIfPresent( 0, 3 ) == '1'

		assert ri.getUrlPartIfPresent( 1, 0 ) == '2'
		assert ri.getUrlPartIfPresent( 1, 2 ) == '2'
		assert ri.getUrlPartIfPresent( 1, 3 ) == '2'
	}

	@Test
	public void testUrlPartRetrieval() {
		Closure testNamespace = { String url ->
			return new RequestInformation( fakeRequest( url ) ).getNamespace()
		}

		Closure testVersion = { String url ->
			return new RequestInformation( fakeRequest( url ) ).getVersion()
		}

		Closure testAction = { String url ->
			return new RequestInformation( fakeRequest( url ) ).getAction()
		}
		assert testNamespace( '/1/namespace/action' ) == 'namespace'
		assert testNamespace( '/namespace/action' ) == 'namespace'
		assert testNamespace( '/action' ) == Event.DEFAULT_NAMESPACE

		assert testVersion( '/2/namespace/action' ) == '2'
		assert testVersion( '/v1/namespace/action' ) == 'v1'
		assert testVersion( '/namespace/action' ) == RequestInformation.DEFAULT_VERSION
		assert testVersion( '/action' ) == RequestInformation.DEFAULT_VERSION

		assert testAction( '/1/namespace/action' ) == 'action'
		assert testAction( '/namespace/action' ) == 'action'
		assert testAction( '/action' ) == 'action'
	}

	private HttpServletRequest fakeRequest( String url ) {
		return [ getPathInfo: { return url } ] as HttpServletRequest
	}


}
